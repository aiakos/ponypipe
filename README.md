# ponypipe

Ponyfill for [the pipeline operator](https://github.com/tc39/proposal-pipeline-operator) (`'Hello world!' |> console.log`)

## Installation and Usage

```sh
npm install ponypipe
```

### pipe(sth, ...) syntax
```js
import pipe from 'ponypipe'
# or
const { pipe } = require('ponypipe')

pipe('Hello world', x => x + '!', console.log)
```

### sth.pipe(...) syntax
```js
import { install } from 'ponypipe'
# or
const { install } = require('ponypipe')

install()

'Hello world'.pipe(x => x + '!', console.log)
```

Installation extends `Object.prototype` - if you don't want this to happen, use the `pipe(sth, ...)` syntax.
