export function pipe (val, ...operators) {
	return operators.reduce((val, fn) => fn(val), val)
}

export default pipe

export function install () {
	Object.defineProperty(Object.prototype, 'pipe', {
		get: () => function (...operators) {
			return pipe(this, ...operators)
		},
		set: () => {},
		configurable: true,
	})
}
