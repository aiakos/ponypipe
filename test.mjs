import pipe, { install } from './index.mjs'

pipe('Hello', console.log)

install()

'world'.pipe(x => x + '!', console.log)
